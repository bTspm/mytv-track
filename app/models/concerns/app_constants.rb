module AppConstants

  DEFAULT_IMAGE = 'https://s-media-cache-ak0.pinimg.com/originals/d3/8b/c3/d38bc38ad9ba60f9091aa2a9b3f4190f.png'
  DEFAULT_REF_DATA = 'N/A'
  INDEX_POSTER_SIZE = '185x272'

  #Different Types
  TOP_RATED = 'top-rated'
  MOST_POPULAR = 'most-popular'
  LATEST_SHOWS = 'latest-shows'

  PAGINATION_COUNT = 24

  #Media Type
  POSTER = 1
  BACKDROP = 2
  TRAILER = 3

  #Gender
  FEMALE = 1
  MALE = 2
end