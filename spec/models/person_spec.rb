# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  tmdb_person_id :integer          not null
#  image_url      :string
#  gender         :integer
#  dob            :date
#  death_date     :date
#  place_of_birth :string
#  biography      :string
#  homepage       :string
#  popularity     :integer          default("0")
#

require 'spec_helper'

describe Person do

  subject(:person) { create :person }

  describe '#record validity' do
    it 'should not be valid - presence' do
      errors =  ["Name can't be blank", "Tmdb person can't be blank"]
      record = build(:person, name: nil, tmdb_person_id: nil, image_url: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array  errors
    end

    it 'should not be valid - uniqueness' do
      person = create :person, tmdb_person_id: 1
      record = build(:person, tmdb_person_id: person.tmdb_person_id, name: person.name)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Name has already been taken', 'Tmdb person has already been taken']
    end

    it 'should be valid' do
      expect(build(:person, name: 'Test - 1', tmdb_person_id: 2, image_url: 'a')).to be_valid
    end
  end

end
