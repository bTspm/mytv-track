require 'spec_helper'
require 'storage/allocator'

RSpec.describe Storage::Allocator do

  subject {Storage::Allocator.new}

  it {should respond_to :tmdb_store}
  it {should respond_to :ref_data_store}
  it {should respond_to :show_store}
  it {should respond_to :person_store}

end