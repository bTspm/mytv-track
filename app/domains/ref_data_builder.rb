class RefDataBuilder

  def init_record_from_response(name:, ref_type:)
    init_record(ref_type: ref_type).tap {|r|
      r.name = name
    }
  end

  def get_ref_data_from_response(response:)
    {
        country: response['origin_country'].try(:first),
        language: response['original_language'],
        type: response['type'],
        status: response['status'],
        genres: extract_names(response: response['genres']),
        production_companies: extract_names(response: response['production_companies']),
        networks: extract_names(response: response['networks']),
    }
  end

  private

  def extract_names(response:)
    return unless response.present?
    response.map {|r| r['name']}
  end

  def init_record(ref_type:)
    ref_type.new
  end

end