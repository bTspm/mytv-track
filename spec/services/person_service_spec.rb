require 'spec_helper'
require 'person_service'

RSpec.describe Services::PersonService do

  let!(:service) {Services::PersonService.new}
  let!(:show) {create :show}
  let!(:show_response) {JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/single_show_response.json")).with_indifferent_access}

  describe '#get_credits_from_response' do
    context 'creators' do
      it 'should create a person and people_show record' do
        creator_response = FakeTmdbStore.new.get_tmdb_record(tmdb_person_id: 123)
        allow_any_instance_of(Storage::TmdbStore).to receive(:get_tmdb_record).and_return(creator_response)
        response = {created_by: show_response[:created_by]}.with_indifferent_access

        #Records Creation
        expect {service.get_credits_from_response(show: show, response: response)}
            .to change {Person.count}.by(1)
                    .and change {PeopleShow.count}.by(1)

        #Validating Created Records
        person = Person.find_by_tmdb_person_id(123)
        expect(person.name).to eq 'ABC'

        #Creator - Record Created
        expect(PeopleShow.where(person_id: person.id, show_id: show.id).first.tmdb_credit_id).to eq PeopleShow::CREATOR
      end

      it 'should update the person and people_show record' do
        person = create :person, name: 'DEF', tmdb_person_id: 100
        create :people_show, person: person, show: show, tmdb_credit_id: PeopleShow::CREATOR

        expect(person.name).to eq 'DEF'

        creator_response = FakeTmdbStore.new.get_tmdb_record(tmdb_person_id: 100)
        allow_any_instance_of(Storage::TmdbStore).to receive(:get_tmdb_record).and_return(creator_response)
        creator = show_response[:created_by][0]
        response = {created_by: [creator.merge!({id: 100})]}.with_indifferent_access

        #Records Updating
        expect {service.get_credits_from_response(show: show, response: response)}
            .to change {Person.count}.by(0)
                    .and change {PeopleShow.count}.by(0)

        #Validating Created Records
        person1 = Person.find_by_tmdb_person_id(100)
        expect(person1.name).to eq 'XYZ'

        #Creator - Record Created
        expect(PeopleShow.where(person_id: person.id, show_id: show.id).first.tmdb_credit_id).to eq PeopleShow::CREATOR
      end
    end

    context 'credits - cast and crew' do
      it 'should create a person and people_show record' do
        credit_response = FakeTmdbStore.new.get_tmdb_record(tmdb_person_id: 123)
        allow_any_instance_of(Storage::TmdbStore).to receive(:get_tmdb_record).and_return(credit_response)
        cast = [show_response[:credits][:cast][0]]
        response = {credits: {cast: cast}}.with_indifferent_access

        #Records Creation
        expect {service.get_credits_from_response(show: show, response: response)}
            .to change {Person.count}.by(1)
                    .and change {PeopleShow.count}.by(1)

        #Validating Created Records
        person = Person.find_by_tmdb_person_id(123)
        expect(person.name).to eq 'ABC'

        #Cast or Crew - Record Created
        expect(PeopleShow.where(person_id: person.id, show_id: show.id).first.tmdb_credit_id).to eq cast[0][:credit_id]
      end

      it 'should update the person and people_show record' do
        person = create :person, name: 'DEF', tmdb_person_id: 100
        create :people_show, person: person, show: show, tmdb_credit_id: 123

        expect(person.name).to eq 'DEF'

        credit_response = FakeTmdbStore.new.get_tmdb_record(tmdb_person_id: 100)
        allow_any_instance_of(Storage::TmdbStore).to receive(:get_tmdb_record).and_return(credit_response)
        cast = [show_response[:credits][:cast][0].merge!({id: 100, credit_id: '123'})]
        response = {credits: {cast: cast}}.with_indifferent_access

        #Records Creation
        expect {service.get_credits_from_response(show: show, response: response)}
            .to change {Person.count}.by(0)
                    .and change {PeopleShow.count}.by(0)

        #Validating Created Records
        person = Person.find_by_tmdb_person_id(100)
        expect(person.name).to eq 'XYZ'

        #Cast or Crew - Record Created
        expect(PeopleShow.where(person_id: person.id, show_id: show.id).first.tmdb_credit_id).to eq cast[0][:credit_id]
      end
    end
  end

end