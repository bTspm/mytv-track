# == Schema Information
#
# Table name: genres
#
#  id   :integer          not null, primary key
#  name :string           not null
#

describe Genre do

  subject(:genre) { create :genre }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:genre, name: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Name can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      create :genre, name: 'TTT'
      record = build(:genre, name: 'TTT')
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Name has already been taken']
    end

    it 'should be valid' do
      expect(build(:genre, name: 'TTT1')).to be_valid
    end
  end

end
