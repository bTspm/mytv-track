require 'spec_helper'

describe ShowsController do

  describe '#index' do
    it 'should list all the shows and render index template' do
      allow_any_instance_of(Services::ShowsService).to receive(:get_shows).and_return('dummy shows')
      allow_any_instance_of(Services::RefDataService).to receive(:get_ref_data_lists).and_return('dummy value')

      get :index
      expect(assigns(:shows)).to eq 'dummy shows'
      expect(assigns(:filter_lists)).to eq 'dummy value'
      expect(response).to render_template 'shows/index'
    end
  end

end