module Services
  class RefDataService < BusinessService
    include Services

    attr_accessor :builder

    def get_ref_data(response:)
      @builder = RefDataBuilder.new
      ref_values = @builder.get_ref_data_from_response(response: response)
      {
          country: ref_data_storage.get_record(iso: ref_values[:country], ref_type: Country),
          language: ref_data_storage.get_record(iso: ref_values[:language], ref_type: Language),
          type: find_or_save_record(name: ref_values[:type], ref_type: Type),
          status: find_or_save_record(name: ref_values[:status], ref_type: Status),
          genres: get_ref_data_collection(collection: ref_values[:genres], ref_type: Genre),
          production_companies: get_ref_data_collection(collection: ref_values[:production_companies], ref_type: ProductionCompany),
          networks: get_ref_data_collection(collection: ref_values[:networks], ref_type: Network),
      }
    end

    def get_ref_data_lists
      {
          genres: ref_data_storage.get_ref_data(ref_type: Genre),
          countries: ref_data_storage.get_ref_data(ref_type: Country),
          types: ref_data_storage.get_ref_data(ref_type: Type),
          statuses: ref_data_storage.get_ref_data(ref_type: Status),
          languages: ref_data_storage.get_ref_data(ref_type: Language),
          networks: ref_data_storage.get_ref_data(ref_type: Network),
      }
    end

    private

    def get_ref_data_collection(collection:, ref_type:)
      return [] unless collection
      collection.map {|name| find_or_save_record(name: name, ref_type: ref_type)}
    end

    def find_or_save_record(name:, ref_type:)
      #:country.to_s.classify.constantize = Country
      record = ref_data_storage.get_record(ref_type: ref_type, name: name)
      return record if record
      record = builder.init_record_from_response(name: name, ref_type: ref_type)
      ref_data_storage.save_record(record: record)
    end

  end
end
