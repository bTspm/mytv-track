class CreatePerson < ActiveRecord::Migration
  def up
    create_table :people do |t|
      t.column :name, :string, null: false
      t.column :tmdb_person_id, :integer, null: false
      t.column :image_url, :string
      t.column :gender, :integer
      t.column :dob, :date
      t.column :death_date, :date
      t.column :place_of_birth, :string
      t.column :biography, :string
      t.column :homepage, :string
      t.column :popularity, :integer, default: 0
    end
  end

  def down
    drop_table :people if table_exists? :people
  end
end
