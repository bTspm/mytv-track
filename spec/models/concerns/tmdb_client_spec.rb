require 'spec_helper'
require 'tmdb_client'

RSpec.describe TmdbClient do

  let!(:client) { TmdbClient.new }
  let!(:url) { 'http://api.themoviedb.org/3/tv/1?api_key=e6fb3428896c8edac5d597bc8188fbb2&append_to_response=credits,videos,images' }

  describe '#get_tmdb_record' do
    it 'should return the Show response' do
      json_response = File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/single_show_response.json")
      stub_request(:get, url).to_return(body: json_response)
      response = client.get_tmdb_record(tmdb_show_id: 1)
      expect(response).to eq JSON.parse(json_response)
    end

    it 'should return the Person response' do
      json_response = File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/person_response.json")
      stub_request(:get, 'http://api.themoviedb.org/3/person/1?api_key=e6fb3428896c8edac5d597bc8188fbb2').to_return(body: json_response)
      response = client.get_tmdb_record(tmdb_person_id: 1)
      expect(response).to eq JSON.parse(json_response)
    end

    it 'should raise error when status' do
      TmdbClient::API_ERRORS.each do |status|
        stub_request(:get, url).to_return(status: status, body: {}.to_json)
        expect{ client.get_tmdb_record(tmdb_show_id: 1) }.to raise_error(Exceptions::AppExceptions::ApiResponseError)
      end
    end

    it 'should raise error when status is 500' do
      stub_request(:get, url).to_return(status: TmdbClient::LIMIT_REACHED, body: {}.to_json)
      expect{ client.get_tmdb_record(tmdb_show_id: 1) }.to raise_error(Exceptions::AppExceptions::RateLimitError)
    end
  end
end