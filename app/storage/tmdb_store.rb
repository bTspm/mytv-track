module Storage

  class TmdbStore
    def get_tmdb_record(tmdb_show_id: nil, tmdb_person_id: nil)
      return unless tmdb_show_id || tmdb_person_id
      TmdbClient.new.get_tmdb_record(tmdb_show_id: tmdb_show_id, tmdb_person_id: tmdb_person_id)
    end
  end
end