# == Schema Information
#
# Table name: networks
#
#  id   :integer          not null, primary key
#  name :string           not null
#

require 'spec_helper'

describe Network do

  subject(:network) { create :network }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:network, name: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Name can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      create :network, name: 'TTT'
      record = build(:network, name: 'TTT')
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Name has already been taken']
    end

    it 'should be valid' do
      expect(build(:network, name: 'TTT1')).to be_valid
    end
  end

end
