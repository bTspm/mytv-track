# == Schema Information
#
# Table name: media
#
#  id         :integer          not null, primary key
#  path       :string           not null
#  show_id    :integer
#  media_type :integer          not null
#

describe Medium do

  subject(:medium) { create :medium }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:medium, path: nil, media_type: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages.uniq).to match_array ["Media type can't be blank", "Path can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      record = build(:medium, path: medium.path, media_type: medium.media_type, show_id: medium.show_id )
      expect(record.valid?).to be false
      expect(record.errors.full_messages.uniq).to match_array ["Show has already been taken"]
    end

    it 'should be valid' do
      expect(build(:medium, path: 'Test - 1', media_type: 1, show_id: 1)).to be_valid
    end
  end

end
