# == Schema Information
#
# Table name: languages
#
#  id   :integer          not null, primary key
#  name :string           not null
#  iso  :string           not null
#

FactoryGirl.define do
  factory :language do
    name 'English'
    iso 'en'
  end
end
