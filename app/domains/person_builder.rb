class PersonBuilder

  def person_from_response(response:, person: nil)
    person ||= initialize_person
    person.tap {|r|
      r.name = response['name']
      r.tmdb_person_id = response['id']
      r.image_url = response['profile_path']
      r.dob = response['birthday']
      r.death_date = response['deathday']
      r.place_of_birth = response['place_of_birth']
      r.biography = response['biography']
      r.gender = response['gender']
      r.popularity = response['popularity'].try(:round)
      r.homepage = response['homepage'].presence
    }
  end

  def build_person_show(person:, show:, person_show:, credits: {})
    person_show ||= initialize_person_show
    person_show.tap {|r|
      r.person_id = person.id
      r.show_id = show.id
      r.tmdb_credit_id = credits[:id]
      r.character = credits[:character]
      r.job = credits[:job]
    }
  end

  def assign_credits(person_response:, credit_id:)
    return {id: credit_id} if credit_id == PeopleShow::CREATOR
    {
        id:  person_response['credit_id'],
        job: person_response['job'],
        character: person_response['character']
    }
  end

  private

  def initialize_person
    Person.new
  end

  def initialize_person_show
    PeopleShow.new
  end

end