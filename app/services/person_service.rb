module Services
  class PersonService < BusinessService
    include Services

    attr_accessor :builder

    def get_credits_from_response(response:, show:)
      @builder = PersonBuilder.new
      save_or_update_credits(show: show, response: response['created_by'], credit_id: PeopleShow::CREATOR) if response['created_by'].present?
      response['credits'].values.each do |credit_response|
        save_or_update_credits(show: show, response: credit_response)
      end if response['credits'].present?
    end

    private

    def save_or_update_credits(show:, response:, credit_id: nil)
      response.each do |person_response|
        person = find_or_save_person(tmdb_person_id: person_response['id'])
        credits = builder.assign_credits(person_response: person_response, credit_id: credit_id)
        find_or_save_person_show(show: show,
                                 person: person,
                                 credits: credits)
      end
    end

    def find_or_save_person(tmdb_person_id:)
      response = tmdb_storage.get_tmdb_record(tmdb_person_id: tmdb_person_id)
      person = person_storage.get_person(tmdb_person_id: tmdb_person_id)
      person = builder.person_from_response(response: response, person: person)
      person_storage.save_record(record: person)
    end

    def find_or_save_person_show(show:, person:, credits: {})
      person_show = person_storage.get_person_show(tmdb_credit_id: credits[:id], show_id: show.id, person_id: person.id)
      person_show = builder.build_person_show(person_show: person_show, person: person, show: show, credits: credits)
      person_storage.save_record(record: person_show)
    end
  end
end
