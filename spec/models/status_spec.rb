# == Schema Information
#
# Table name: statuses
#
#  id   :integer          not null, primary key
#  name :string           not null
#

require 'spec_helper'

describe Status do

  subject(:status) { create :status }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:status, name: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Name can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      create :status, name: 'TTT'
      record = build(:status, name: 'TTT')
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Name has already been taken']
    end

    it 'should be valid' do
      expect(build(:status, name: 'TTT1')).to be_valid
    end
  end

end
