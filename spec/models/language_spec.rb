# == Schema Information
#
# Table name: languages
#
#  id   :integer          not null, primary key
#  name :string           not null
#  iso  :string           not null
#

describe Language do

  subject(:language) { create :language }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:language, name: nil, iso: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Iso can't be blank", "Name can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      record = build(:language, name: language.name, iso: language.iso )
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Iso has already been taken", "Name has already been taken"]
    end

    it 'should be valid' do
      expect(build(:language, name: 'Test - 1')).to be_valid
    end
  end

end
