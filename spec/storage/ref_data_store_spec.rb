require 'spec_helper'
require 'show_store'

RSpec.describe Storage::RefDataStore do

  let!(:store) { Storage::RefDataStore.new }
  let!(:country) { create :country }

  describe '#get_record' do
    it 'should return record by name' do
      expect(store.get_record(ref_type: Country, name: country.name).id).to eq country.id
      expect(store.get_record(ref_type: Country, name: 'TTT')).to be_nil
    end

    it 'should return record by iso' do
      expect(store.get_record(ref_type: Country, iso: country.iso).id).to eq country.id
      expect(store.get_record(ref_type: Country, iso: country.iso + 'a')).to be_nil
    end

    it 'should return nil if no search paaramter is passed' do
      expect(store.get_record(ref_type: Country)).to be_nil
    end
  end

  describe '#save_record' do
    it 'should create record' do
      country = Country.new({ name: 'Country Name', iso: 'TEST' })
      expect{ store.save_record(record: country) }.to change{ Country.count }.by 1
      expect(Country.find_by_iso('TEST').name).to eq 'Country Name'
    end

    it 'should update the record' do
      expect(country.name).to eq 'United States'
      country.name = 'Country Name'
      expect{ store.save_record(record: country) }.to change{ Country.count }.by 0
      expect(Country.find_by_iso(country.iso).name).to eq 'Country Name'
    end
  end

  describe '#get_ref_data' do
    it 'should return the collection based on ref_type' do
      create :country, name: 'ZZZ', iso: 'ZZZ'
      result = store.get_ref_data(ref_type: Country)
      expect(result.first.name).to eq 'United States'
      expect(result.last.name).to eq  'ZZZ'
    end
  end

end
