class CreateGenresShows < ActiveRecord::Migration
  def up
    create_table :genres_shows do |t|
      t.integer :show_id
      t.integer :genre_id
    end
  end

  def down
    drop_table :genres_shows if table_exists? :genres_shows
  end
end

