class CreateMedium < ActiveRecord::Migration
  def up
    create_table :media do |t|
      t.column :path, :string, null: false
      t.column :show_id, :integer
      t.column :media_type, :integer, null: false
    end
  end

  def down
    drop_table :media if table_exists? :media
  end
end
