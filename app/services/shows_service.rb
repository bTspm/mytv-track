module Services
  class ShowsService < BusinessService
    include Services

    def get_single_show(tmdb_show_id:)
      response = tmdb_storage.get_tmdb_record(tmdb_show_id: tmdb_show_id)
      show = show_storage.get_show(tmdb_show_id: response['id'])
      save_or_update_show(show: show, response: response)
    end

    def get_shows(filter_params: {}, options: {})
      show_storage.get_shows(options: options, filter_params: filter_params)
    end

    private

    def save_or_update_show(show:, response:)
      ActiveRecord::Base.transaction do
        ref_data = ref_data_service.get_ref_data(response: response)
        show = ShowBuilder.new.from_response(response: response, ref_data: ref_data, show: show)
        show = show_storage.save_record(record: show)
        person_service.get_credits_from_response(response: response, show: show)
        show_media(show: show, response: response)
      end
      show
    end

    def show_media(show:, response:)
      backdrop = response['images']['backdrops'].first(6) if response['images'] && response['images']['backdrops'].present?
      trailer = response['videos']['results'].find {|video| video['type'] == 'Trailer'} if response['videos'] && response['videos']['results'].present?
      media = {
          AppConstants::POSTER => response['poster_path'].presence,
          AppConstants::BACKDROP => backdrop.try(:to_json),
          AppConstants::TRAILER => trailer.try(:[],'key')
      }
      media_builder = MediaBuilder.new
      media.each do |k, v|
        save_or_update_media(show: show, path: v, media_type: k, builder: media_builder) if v
      end
    end

    def save_or_update_media(show:, path:, media_type:, builder:)
      media = show_storage.get_show_media(show: show, media_type: media_type)
      media = builder.media_from_response(media: media, show_id: show.id, path: path, media_type: media_type)
      show_storage.save_record(record: media)
    end

  end
end