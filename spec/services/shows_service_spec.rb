require 'spec_helper'
require 'shows_service'

RSpec.describe Services::ShowsService do

  #let!(:user) { create :user } TODO: After user model is updated. and pass user
  let!(:service) { Services::ShowsService.new }
  let!(:show) { create :show }
  let!(:country) {create :country}
  let!(:language) {create :language}
  let!(:status) {create :status}
  let!(:type) {create :type}
  let!(:genre) {create :genre}
  let!(:production_company) {create :production_company}
  let!(:network) {create :network}
  let!(:ref_data) { { country: country, language: language, type: type, status: status,
                      genres: [genre], production_companies: [production_company], networks: [network] } }

  describe '#get_tmdb_record' do
    before :each do
      allow_any_instance_of(Services::RefDataService).to receive(:get_ref_data).and_return(ref_data)
      allow_any_instance_of(Services::PersonService).to receive(:get_credits_from_response).and_return(true)
    end
    it 'should save show if not exists' do
      response = JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/single_show_response.json")).with_indifferent_access
      allow_any_instance_of(Storage::TmdbStore).to receive(:get_tmdb_record).and_return(response.merge!({id: 123}))
      expect{ service.get_single_show(tmdb_show_id: 123) }
          .to change{ Show.count }.by(1)
          .and change{ Medium.count }.by 3
    end

    it 'should update and return show' do
      response = JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/single_show_response.json")).with_indifferent_access
      media = {
          AppConstants::POSTER => 'A',
          AppConstants::BACKDROP => 'B',
          AppConstants::TRAILER => 'C'
      }
      media.each do |k, v|
        create :medium, show_id: show.id, media_type: k, path: v
      end
      media_paths = Medium.all.map(&:path)
      allow_any_instance_of(Storage::TmdbStore).to receive(:get_tmdb_record).and_return(response.merge!({name: 'ABC', id: show.tmdb_show_id}))
      expect(show.name).to eq 'Test Show'
      show1 = service.get_single_show(tmdb_show_id: show.tmdb_show_id)
      expect(show1.name).to eq 'ABC' #After Update
      Medium.all.each do |medium|
        media_paths.exclude? expect(medium.path) #After Update
      end
    end
  end

  describe '#get_shows' do
    it 'should return shows based on parama and options' do
      allow_any_instance_of(Storage::ShowStore).to receive(:get_shows).and_return('dummy value')
      expect(service.get_shows).to eq 'dummy value'
    end
  end

end