# == Schema Information
#
# Table name: production_companies
#
#  id   :integer          not null, primary key
#  name :string           not null
#

FactoryGirl.define do
  factory :production_company do
    name 'ATM'
  end
end
