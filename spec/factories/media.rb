# == Schema Information
#
# Table name: media
#
#  id         :integer          not null, primary key
#  path       :string           not null
#  show_id    :integer
#  media_type :integer          not null
#

FactoryGirl.define do
  factory :medium do
    show_id 1
    path 'image path'
    media_type 1
  end
end
