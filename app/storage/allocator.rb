module Storage

  class Allocator
    def tmdb_store
      Storage::TmdbStore.new
    end

    def ref_data_store
      Storage::RefDataStore.new
    end

    def show_store
      Storage::ShowStore.new
    end

    def person_store
      Storage::PersonStore.new
    end
  end
end