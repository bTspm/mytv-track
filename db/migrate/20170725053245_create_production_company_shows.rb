class CreateProductionCompanyShows < ActiveRecord::Migration
  def up
    create_table :production_companies_shows do |t|
      t.integer :production_company_id
      t.integer :show_id
    end
  end

  def down
    drop_table :production_companies_shows if table_exists? :production_companies_shows
  end

end
