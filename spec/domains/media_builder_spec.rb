require 'spec_helper'

RSpec.describe MediaBuilder do

  let!(:builder) {MediaBuilder.new}
  let!(:media) { create :medium, media_type: AppConstants::POSTER }

  describe '#media_from_response' do
    it 'should return media with values passed' do
      result = builder.media_from_response(media: media, show_id: 23, path: 'ABC', media_type: AppConstants::TRAILER)
      expect(result.show_id).to eq 23
      expect(result.path).to eq 'ABC'
      expect(result.media_type).to eq AppConstants::TRAILER
    end

    it 'should initialize new show if none passed' do
      result = builder.media_from_response(show_id: 1, path: 'ABC', media_type: 1)
      expect(result.new_record?).to be true
    end
  end
end
