# == Schema Information
#
# Table name: networks
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Network < ActiveRecord::Base
  has_and_belongs_to_many :shows

  validates :name, presence: true, uniqueness: true
end
