# == Schema Information
#
# Table name: types
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Type < ActiveRecord::Base
  has_many :shows

  validates :name, presence: true, uniqueness: true
end
