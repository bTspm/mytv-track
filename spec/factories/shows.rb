# == Schema Information
#
# Table name: shows
#
#  id            :integer          not null, primary key
#  tmdb_show_id  :integer          not null
#  name          :string           not null
#  country_id    :integer
#  language_id   :integer
#  status_id     :integer
#  type_id       :integer
#  rating        :decimal(4, 2)
#  start_date    :date
#  end_date      :date
#  summary       :string
#  runtime       :integer
#  popularity    :integer          default("0")
#  in_production :boolean          default("f")
#  homepage      :string
#  created_at    :datetime
#  updated_at    :datetime
#

FactoryGirl.define do
  factory :show do
    name 'Test Show'
    tmdb_show_id 1
    country_id 1
    start_date Date.today
    rating '1.0'
    language_id 1
    status_id 1
    type_id 1
    summary 'Test'
    runtime 60
    popularity 60
    in_production true
    created_at DateTime.now
    updated_at DateTime.now + 1.day
  end
end
