#Todo move this to Service layer
class ApplicationStartup

  def initialize(config_file_path:, environment:)
    @config_path = config_file_path
    @env = environment
  end

  def establish_env_variables_hash
    Rails.logger.info '***** Loading Environment Specific Properties *****'
    Rails.logger.info "config_file_path=#{config_path}"
    env_yaml_path = Rails.root.join(config_path) # The path to the .yml file
    env_yaml = ERB.new( env_yaml_path.read ).result # Read the file and evaluate the ERB
    full_hash = YAML.load(env_yaml) # Parse the resulting YAML
    env_hash = full_hash[env]
    Rails.logger.info "ENV_PROPS has been loaded for Rails.env=#{env}"
    env_hash
  end

  private

  attr_reader :config_path, :env

end