# == Schema Information
#
# Table name: countries
#
#  id   :integer          not null, primary key
#  name :string           not null
#  iso  :string           not null
#

require 'spec_helper'

describe Country do

  subject(:country) { create :country }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:country, name: nil, iso: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Iso can't be blank", "Name can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      record = build(:country, name: country.name, iso: country.iso )
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Iso has already been taken", "Name has already been taken"]
    end

    it 'should be valid' do
      expect(build(:country, name: 'Test - 1')).to be_valid
    end
  end

end
