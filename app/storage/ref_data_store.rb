module Storage
  class RefDataStore
    include EagerLoadable
    include Storage::Saveable

    def get_record(ref_type:, name: nil, iso: nil)
      return nil unless name || iso
      return ref_type.find_by_name(name) if name
      ref_type.find_by_iso(iso)
    end

    def save_record(record:)
      _save(record: record)
    end

    def get_ref_data(ref_type:)
      ref_type.order(name: :asc)
    end

  end
end