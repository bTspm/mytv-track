class CreateNetwork < ActiveRecord::Migration
  def up
    create_table :networks do |t|
      t.column :name, :string, null: false
    end
  end

  def down
    drop_table :networks if table_exists? :networks
  end
end
