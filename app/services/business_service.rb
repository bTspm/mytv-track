module Services
  class BusinessService
    attr_accessor :user

    def initialize(user: nil, engine: nil)
      @engine = engine
      @user = user
    end

    def engine
      #FakeStorage can be initialized for tests.
      @engine ||= ::Storage::Allocator.new
    end

    def current_user
      @user
    end

    def tmdb_storage
      engine.tmdb_store
    end

    def ref_data_storage
      engine.ref_data_store
    end

    def show_storage
      engine.show_store
    end

    def person_storage
      engine.person_store
    end
  end
end