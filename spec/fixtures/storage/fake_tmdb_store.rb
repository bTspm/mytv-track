class FakeTmdbStore

  def get_tmdb_record(tmdb_person_id: nil, tmdb_show_id: nil)
    person_reponse(tmdb_person_id: tmdb_person_id) if tmdb_person_id
  end

  private

  def person_reponse(tmdb_person_id:)
    person_response = JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/person_response.json")).with_indifferent_access
    name = tmdb_person_id == 123 ? 'ABC' : 'XYZ' #Using id 123 for testing create cases and other for updating cases.
    person_response.merge!({id: tmdb_person_id, credit_id: tmdb_person_id, name: name})
    person_response
  end

end
