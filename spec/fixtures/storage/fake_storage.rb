class FakeStorage < Storage::Allocator

  def tmdb_store
    ::FakeTmdbStore.new
  end

end