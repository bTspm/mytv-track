# == Schema Information
#
# Table name: countries
#
#  id   :integer          not null, primary key
#  name :string           not null
#  iso  :string           not null
#

class Country < ActiveRecord::Base
  has_many :shows

  validates :name, :iso, presence: true, uniqueness: true
end
