require 'spec_helper'
require 'tmdb_store'

RSpec.describe Storage::TmdbStore do

  let!(:store) {Storage::TmdbStore.new}

  describe '#get_single_show' do
    it 'should return response from api' do
      #Show
      allow_any_instance_of(TmdbClient).to receive(:get_tmdb_record).and_return('dummy value')
      expect(store.get_tmdb_record(tmdb_show_id: 1)).to eq 'dummy value'

      #Person
      allow_any_instance_of(TmdbClient).to receive(:get_tmdb_record).and_return('dummy value')
      expect(store.get_tmdb_record(tmdb_person_id: 1)).to eq 'dummy value'

      #No Paramater
      expect(store.get_tmdb_record).to be_nil
    end
  end

end