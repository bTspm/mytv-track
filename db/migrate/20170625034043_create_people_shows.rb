class CreatePeopleShows < ActiveRecord::Migration
  def up
    create_table :people_shows do |t|
      t.column :person_id, :integer, null: false
      t.column :show_id, :integer, null: false
      t.column :tmdb_credit_id, :string, null: false
      t.column :character, :string
      t.column :job, :string
    end
  end

  def down
    drop_table :people_shows if table_exists? :people_shows
  end
end
