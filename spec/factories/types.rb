# == Schema Information
#
# Table name: types
#
#  id   :integer          not null, primary key
#  name :string           not null
#

FactoryGirl.define do
  factory :type do
    name 'Reality'
  end
end
