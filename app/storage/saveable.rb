module Storage
  module Saveable

    def _save(record:)
      record.save!
      record
    rescue ActiveRecord::RecordInvalid, ActiveRecord::StatementInvalid
      raise Exceptions::AppExceptions::RecordInvalid.new(record: record)
    end

  end
end