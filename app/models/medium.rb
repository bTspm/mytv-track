# == Schema Information
#
# Table name: media
#
#  id         :integer          not null, primary key
#  path       :string           not null
#  show_id    :integer
#  media_type :integer          not null
#

class Medium < ActiveRecord::Base
  belongs_to :show

  validates :path, :media_type, presence: true
  validates :show_id, uniqueness: { scope: :media_type }
end
