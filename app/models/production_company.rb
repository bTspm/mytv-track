# == Schema Information
#
# Table name: production_companies
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class ProductionCompany < ActiveRecord::Base
  has_and_belongs_to_many :shows

  validates :name, presence: true, uniqueness: true
end
