# == Schema Information
#
# Table name: shows
#
#  id            :integer          not null, primary key
#  tmdb_show_id  :integer          not null
#  name          :string           not null
#  country_id    :integer
#  language_id   :integer
#  status_id     :integer
#  type_id       :integer
#  rating        :decimal(4, 2)
#  start_date    :date
#  end_date      :date
#  summary       :string
#  runtime       :integer
#  popularity    :integer          default("0")
#  in_production :boolean          default("f")
#  homepage      :string
#  created_at    :datetime
#  updated_at    :datetime
#

class Show < ActiveRecord::Base
  belongs_to :country
  belongs_to :language
  belongs_to :status
  belongs_to :type
  belongs_to :network

  has_many :media
  has_one :poster, -> { where(media_type: AppConstants::POSTER) }, class_name: Medium
  has_one :backdrop, -> { where(media_type: AppConstants::BACKDROP) }, class_name: Medium
  has_one :trailer, -> { where(media_type: AppConstants::TRAILER) }, class_name: Medium

  has_many :people_shows
  has_many :people, through: :people_shows
  has_and_belongs_to_many :genres
  has_and_belongs_to_many :networks
  has_and_belongs_to_many :production_companies

  validates :tmdb_show_id, :name, presence: true, uniqueness: true
end
