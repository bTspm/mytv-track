require 'spec_helper'
require 'ref_data_builder'

RSpec.describe RefDataBuilder do

  let!(:builder) {RefDataBuilder.new}
  let!(:country) {create :country}
  let(:response) {JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/single_show_response.json"))}

  describe '#init_record_from_response' do
    it 'should initialize ref_type record' do
      response = builder.init_record_from_response(name: 'Test', ref_type: Country)
      expect(response).to be_kind_of Country
      expect(response.name).to eq 'Test'
      expect(response.new_record?).to be true
    end
  end

  describe '#get_ref_data_from_response' do
    it 'should return ref_values hash from response' do
      result = builder.get_ref_data_from_response(response: response)
      expect(result[:country]).to eq 'US'
      expect(result[:language]).to eq 'en'
      expect(result[:type]).to eq 'Scripted'
      expect(result[:status]).to eq 'Ended'
      expect(result[:genres]).to match_array ['Drama']
      expect(result[:production_companies]).to match_array ['Gran Via Productions', 'Sony Pictures Television', 'High Bridge Entertainment']
      expect(result[:networks]).to match_array ['AMC']
    end
  end
end
