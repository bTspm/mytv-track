# == Schema Information
#
# Table name: production_companies
#
#  id   :integer          not null, primary key
#  name :string           not null
#

require 'spec_helper'

describe Network do

  subject(:production_company) { create :production_company }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:production_company, name: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Name can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      create :production_company, name: 'Test'
      record = build(:production_company, name: 'Test')
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Name has already been taken']
    end

    it 'should be valid' do
      expect(build(:production_company, name: 'Test - 1')).to be_valid
    end
  end

end
