# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170806044526) do

  create_table "countries", force: :cascade do |t|
    t.string "name", null: false
    t.string "iso",  null: false
  end

  create_table "genres", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "genres_shows", force: :cascade do |t|
    t.integer "show_id"
    t.integer "genre_id"
  end

  create_table "languages", force: :cascade do |t|
    t.string "name", null: false
    t.string "iso",  null: false
  end

  create_table "media", force: :cascade do |t|
    t.string  "path",       null: false
    t.integer "show_id"
    t.integer "media_type", null: false
  end

  create_table "networks", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "networks_shows", force: :cascade do |t|
    t.integer "network_id"
    t.integer "show_id"
  end

  create_table "people", force: :cascade do |t|
    t.string  "name",                       null: false
    t.integer "tmdb_person_id",             null: false
    t.string  "image_url"
    t.integer "gender"
    t.date    "dob"
    t.date    "death_date"
    t.string  "place_of_birth"
    t.string  "biography"
    t.string  "homepage"
    t.integer "popularity",     default: 0
  end

  create_table "people_shows", force: :cascade do |t|
    t.integer "person_id",      null: false
    t.integer "show_id",        null: false
    t.string  "tmdb_credit_id", null: false
    t.string  "character"
    t.string  "job"
  end

  create_table "production_companies", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "production_companies_shows", force: :cascade do |t|
    t.integer "production_company_id"
    t.integer "show_id"
  end

  create_table "shows", force: :cascade do |t|
    t.integer  "tmdb_show_id",                                          null: false
    t.string   "name",                                                  null: false
    t.integer  "country_id"
    t.integer  "language_id"
    t.integer  "status_id"
    t.integer  "type_id"
    t.decimal  "rating",        precision: 4, scale: 2
    t.date     "start_date"
    t.date     "end_date"
    t.string   "summary"
    t.integer  "runtime"
    t.integer  "popularity",                            default: 0
    t.boolean  "in_production",                         default: false
    t.string   "homepage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "types", force: :cascade do |t|
    t.string "name", null: false
  end

end
