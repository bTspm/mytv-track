class CreateStatus < ActiveRecord::Migration
  def up
    create_table :statuses do |t|
      t.column :name, :string, null: false
    end
  end

  def down
    drop_table :statuses if table_exists? :statuses
  end
end
