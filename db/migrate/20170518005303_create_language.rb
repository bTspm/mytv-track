class CreateLanguage < ActiveRecord::Migration
  def up
    create_table :languages do |t|
      t.column :name, :string, null: false
      t.column :iso, :string, null: false
    end
  end

  def down
    drop_table :languages if table_exists? :languages
  end
end
