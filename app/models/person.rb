# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  tmdb_person_id :integer          not null
#  image_url      :string
#  gender         :integer
#  dob            :date
#  death_date     :date
#  place_of_birth :string
#  biography      :string
#  homepage       :string
#  popularity     :integer          default("0")
#

class Person < ActiveRecord::Base

  has_many :people_shows
  has_many :shows, through: :people_shows

  validates :name, :tmdb_person_id, uniqueness: true, presence: true

end
