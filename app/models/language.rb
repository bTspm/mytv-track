# == Schema Information
#
# Table name: languages
#
#  id   :integer          not null, primary key
#  name :string           not null
#  iso  :string           not null
#

class Language < ActiveRecord::Base
  has_many :shows

  validates :name, :iso, presence: true, uniqueness: true
end
