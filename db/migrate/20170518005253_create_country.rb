class CreateCountry < ActiveRecord::Migration
  def up
    create_table :countries do |t|
      t.column :name, :string, null: false
      t.column :iso, :string, null: false
    end
  end

  def down
    drop_table :countries if table_exists? :countries
  end
end
