require 'spec_helper'
require 'person_store'

RSpec.describe Storage::PersonStore do

  let!(:store) { Storage::PersonStore.new }
  let!(:person) { create :person, tmdb_person_id: 1 }
  let!(:show) { create :show, tmdb_show_id: 1 }
  let!(:person_show) { create :people_show, tmdb_credit_id: 'A', person: person, show: show}

  describe '#get_person' do
    it 'should return person on availability' do
      response = store.get_person(tmdb_person_id: 1)
      expect(response.tmdb_person_id).to eq person.tmdb_person_id
      expect(response.name).to eq person.name

      # Passing last + 1 id would not be available.
      expect(store.get_person(tmdb_person_id: Person.last.tmdb_person_id + 1)).to be_nil
    end
  end

  describe '#get_person_show' do
    it 'should return actor_show on availability' do
      response = store.get_person_show(tmdb_credit_id: 'A', show_id: show.id, person_id: person.id)
      expect(response.tmdb_credit_id).to eq person_show.tmdb_credit_id
      expect(response.character).to eq person_show.character

      # Passing last + 1 id would not be available.
      response = store.get_person_show(tmdb_credit_id: PeopleShow.last.tmdb_credit_id + 'A', show_id: show.id, person_id: person.id)
      expect(response).to be_nil
    end
  end

  describe '#save_record' do
    it 'should create and return person' do
      attrs = person.attributes.merge({id: nil, tmdb_person_id: 999, name: 'Test'})
      person = Person.new(attrs)
      expect{ store.save_record(record: person) }.to change{ Person.count }.by 1
      expect(Person.find_by_tmdb_person_id(999).name).to eq 'Test'
    end
    it 'should update and return person' do
      expect(person.name).to eq 'Test Person'
      expect(person.tmdb_person_id).to eq 1
      person.name = 'Test Changed'
      expect{ store.save_record(record: person) }.to change{ Person.count }.by 0
      expect(Person.find_by_tmdb_person_id(1).name).to eq 'Test Changed'
    end
  end

end