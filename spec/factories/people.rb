# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  tmdb_person_id :integer          not null
#  image_url      :string
#  gender         :integer
#  dob            :date
#  death_date     :date
#  place_of_birth :string
#  biography      :string
#  homepage       :string
#  popularity     :integer          default("0")
#

FactoryGirl.define do
  factory :person do
    name 'Test Person'
    tmdb_person_id 1
    image_url 'URL'
    dob Date.today
    death_date Date.today
    place_of_birth 'Test City'
    biography 'Test Biography'
    gender 1
    popularity 2
    homepage 'www.btspm.com'
  end
end
