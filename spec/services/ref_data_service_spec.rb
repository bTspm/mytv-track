require 'spec_helper'
require 'ref_data_service'

RSpec.describe Services::RefDataService do

  let!(:service) {Services::RefDataService.new}
  let(:response) {JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/single_show_response.json")).with_indifferent_access}

  describe '#get_ref_data' do
    it 'should create ref data if records doesnt exist' do
      expect {service.get_ref_data(response: response)}
          .to change {Type.count}.by(1)
          .and change {Status.count}.by(1)
          .and change {Network.count}.by(1)
          .and change {Genre.count}.by(1)
          .and change {ProductionCompany.count}.by(3)
    end
    it 'should search and return hash ids with the ref data records' do
      country = create :country, name: 'United States', iso: 'US'
      language = create :language, name: 'English', iso: 'en'
      type = create :type, name: 'Scripted'
      status = create :status, name: 'Ended'
      network = create :network, name: 'AMC'
      production_company = create :production_company, name: 'Sony Pictures Television'
      genre = create :genre, name: 'Drama'
      result = service.get_ref_data(response: response)
      expect(result[:country]).to eq country
      expect(result[:type]).to eq type
      expect(result[:status]).to eq status
      expect(result[:language]).to eq language
      expect(result[:networks]).to include network
      expect(result[:genres]).to include genre
      expect(result[:production_companies]).to include production_company
    end
  end

  describe '#get_ref_data_lists' do
    it 'should return a hash with ref_data' do
      allow_any_instance_of(Storage::RefDataStore).to receive(:get_ref_data).and_return('dummy value')
      result = service.get_ref_data_lists
      expect(result.keys).to match_array  [:genres, :countries, :types, :statuses, :languages, :networks]
      expect(result.values).to match_array  ['dummy value', 'dummy value', 'dummy value', 'dummy value', 'dummy value', 'dummy value']
    end
  end

end