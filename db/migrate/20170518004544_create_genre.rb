class CreateGenre < ActiveRecord::Migration
  def up
    create_table :genres do |t|
      t.column :name, :string, null: false
    end
  end

  def down
    drop_table :genres if table_exists? :genres
  end
end
