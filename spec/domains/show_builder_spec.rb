require 'spec_helper'
require 'show_builder'

RSpec.describe ShowBuilder do

  let!(:builder) {ShowBuilder.new}
  let!(:show) {create :show}
  let!(:country) {create :country}
  let!(:language) {create :language}
  let!(:status) {create :status}
  let!(:type) {create :type}
  let!(:genre) {create :genre}
  let!(:production_company) {create :production_company}
  let!(:network) {create :network}
  let!(:response) {JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/single_show_response.json"))}
  let!(:ref_data) { { country: country, language: language, type: type, status: status,
                            genres: [genre], production_companies: [production_company], networks: [network] } }

  describe '#from_response' do
    it 'should return show with values from response' do
      result = builder.from_response(response: response, ref_data: ref_data, show: show)
      expect(result.tmdb_show_id).to eq 1396
      expect(result.name).to eq response['name']
      expect(result.country_id).to eq country.id
      expect(result.language_id).to eq language.id
      expect(result.status_id).to eq status.id
      expect(result.type_id).to eq type.id
      expect(result.genres.first.id).to eq genre.id
      expect(result.production_companies.first.id).to eq production_company.id
      expect(result.networks.first.id).to eq network.id
      expect(result.start_date.to_s).to eq '2008-01-19'
      expect(result.end_date.to_s).to eq '2013-09-29'
      expect(result.rating.to_f).to eq 8.2
      expect(result.summary).to eq response['overview']
      expect(result.runtime).to eq 45
      expect(result.popularity).to eq 21
      expect(result.in_production).to be false
      expect(result.homepage).to eq 'http://www.amc.com/shows/breaking-bad'
    end

    it 'should initialize new show if none passed' do
      result = builder.from_response(response: response, ref_data: ref_data, show: nil)
      expect(result.new_record?).to be true
    end
  end
end
