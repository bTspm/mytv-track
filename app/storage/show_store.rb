module Storage
  class ShowStore
    include EagerLoadable
    include Saveable

    def get_show(tmdb_show_id:)
      Show.where(tmdb_show_id: tmdb_show_id).first
    end

    def save_record(record:)
      _save(record: record)
    end

    def get_shows(filter_params: {}, options: {})
      shows = get_shows_sort_by(sort_by: options[:sort_by])
      shows = shows.where('name LIKE :query', query: "%#{options[:search_query]}%") if options[:search_query].present?
      shows = get_shows_by_filter(filter_params: filter_params, shows: shows) if filter_params.present?
      options[:limit] ? shows.limit(options[:limit]) : shows.page(options[:page]).per(AppConstants::PAGINATION_COUNT)
    end

    def get_show_media(show:, media_type:)
      case media_type
        when AppConstants::POSTER
          show.poster
        when AppConstants::TRAILER
          show.trailer
        when AppConstants::BACKDROP
          show.backdrop
      end
    end

    private

    def get_shows_by_filter(filter_params:, shows:)
      shows = shows.where(country_id: filter_params[:country_id]) if filter_params[:country_id].present?
      shows = shows.where(type_id: filter_params[:type_id]) if filter_params[:type_id].present?
      shows = shows.where(language_id: filter_params[:language_id]) if filter_params[:language_id].present?
      shows = shows.where(status_id: filter_params[:status_id]) if filter_params[:status_id].present?
      shows = shows.joins(:networks).where('networks.id =?', filter_params[:network_id]) if filter_params[:network_id].present?
      shows = shows.joins(:genres).where('genres.id =?', filter_params[:genre_id]) if filter_params[:genre_id].present?
      shows = shows.joins(:production_companies).where('production_companies.id =?', filter_params[:production_company_id]) if filter_params[:production_company_id].present?
      shows
    end

    def get_shows_sort_by(sort_by:)
      case sort_by
        when AppConstants::LATEST_SHOWS
          Show.order(start_date: :desc, name: :asc)
        when AppConstants::TOP_RATED
          Show.order(rating: :desc, popularity: :desc)
        when AppConstants::MOST_POPULAR
          Show.order(popularity: :desc, name: :asc)
        else
          Show.order(name: :asc)
      end
    end

  end
end