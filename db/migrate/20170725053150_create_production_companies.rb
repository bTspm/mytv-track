class CreateProductionCompanies < ActiveRecord::Migration
  def up
    create_table :production_companies do |t|
      t.column :name, :string, null: false
    end
  end

  def down
    drop_table :production_companies if table_exists? :production_companies
  end
end
