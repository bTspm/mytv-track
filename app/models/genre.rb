# == Schema Information
#
# Table name: genres
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Genre < ActiveRecord::Base
  has_and_belongs_to_many :shows

  validates :name, presence: true, uniqueness: true
end
