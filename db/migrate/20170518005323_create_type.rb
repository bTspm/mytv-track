class CreateType < ActiveRecord::Migration
  def up
    create_table :types do |t|
      t.column :name, :string, null: false
    end
  end

  def down
    drop_table :types if table_exists? :types
  end
end
