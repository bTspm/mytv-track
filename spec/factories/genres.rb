# == Schema Information
#
# Table name: genres
#
#  id   :integer          not null, primary key
#  name :string           not null
#

FactoryGirl.define do
  factory :genre do
    name 'Action'
  end
end
