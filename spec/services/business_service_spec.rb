require 'spec_helper'
require 'services/business_service'

RSpec.describe Services::BusinessService do

  subject {Services::BusinessService.new}

  it {should respond_to :engine}
  it {should respond_to :current_user}
  it {should respond_to :tmdb_storage}
  it {should respond_to :ref_data_storage}
  it {should respond_to :person_storage}

end