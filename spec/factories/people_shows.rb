# == Schema Information
#
# Table name: people_shows
#
#  id             :integer          not null, primary key
#  person_id      :integer          not null
#  show_id        :integer          not null
#  tmdb_credit_id :string           not null
#  character      :string
#  job            :string
#

FactoryGirl.define do
  factory :people_show do
    person_id 1
    show_id 1
    tmdb_credit_id 1
    character 'Char Name'
    job 'Job Name'
  end
end
