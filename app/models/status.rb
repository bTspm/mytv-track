# == Schema Information
#
# Table name: statuses
#
#  id   :integer          not null, primary key
#  name :string           not null
#

class Status < ActiveRecord::Base
  has_many :shows

  validates :name, presence: true, uniqueness: true
end
