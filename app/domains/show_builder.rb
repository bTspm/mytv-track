class ShowBuilder

  def from_response(response:, ref_data: {}, show: nil)
    show ||= initialize_show
    show.tap { |r|
      r.tmdb_show_id = response['id']
      r.name = response['name']
      r.country = ref_data[:country]
      r.language = ref_data[:language]
      r.status = ref_data[:status]
      r.type = ref_data[:type]
      r.genres = ref_data[:genres]
      r.production_companies = ref_data[:production_companies]
      r.networks = ref_data[:networks]
      r.start_date = response['first_air_date']
      r.end_date = response['last_air_date']
      r.rating = response['vote_average']
      r.summary = response['overview']
      r.runtime = response['episode_run_time'].try(:first)
      r.popularity = response['popularity'].try(:round)
      r.in_production = response['in_production']
      r.homepage = response['homepage'].presence
    }
  end

  private

  def initialize_show
    Show.new
  end
end