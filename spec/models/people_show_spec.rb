# == Schema Information
#
# Table name: people_shows
#
#  id             :integer          not null, primary key
#  person_id      :integer          not null
#  show_id        :integer          not null
#  tmdb_credit_id :string           not null
#  character      :string
#  job            :string
#

require 'spec_helper'

describe PeopleShow do

  subject(:people_show) { create :people_show }

  describe '#record validity' do
    it 'should not be valid - presence' do
      errors = ["Need at-least job or character", "Person can't be blank",
                "Show can't be blank", "Tmdb credit can't be blank"]
      record = build(:people_show, person_id: nil, show_id: nil, tmdb_credit_id: nil, character: nil, job: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array  errors
    end

    it 'should be valid if the credit is Creator without job or character' do
      record = build(:people_show, tmdb_credit_id: PeopleShow::CREATOR, character: nil, job: nil)
      expect(record.valid?).to be true
    end

    it 'should not be valid - uniqueness' do
      create :people_show, tmdb_credit_id: 1
      record = build(:people_show, tmdb_credit_id: 1)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Tmdb credit has already been taken']
    end

    it 'should be valid' do
      expect(build(:people_show, person_id: 2, show_id: 2, tmdb_credit_id: 2,
                   character: 'ABC', job: nil)).to be_valid

      expect(build(:people_show, person_id: 2, show_id: 2, tmdb_credit_id: 2,
                   character: nil, job: 'ABC')).to be_valid
    end

  end

end
