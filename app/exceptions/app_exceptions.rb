module Exceptions
  module AppExceptions
    class RecordInvalid < StandardError
      attr_accessor :record

      def initialize(record:)
        @record = record
      end
    end

    class ApiResponseError < StandardError
      attr_accessor :message

      def initialize(message:)
        @message = message
      end
    end

    class RateLimitError < StandardError;

    end
  end
end