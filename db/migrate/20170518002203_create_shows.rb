class CreateShows < ActiveRecord::Migration
  def up
    create_table :shows do |t|
      t.column :tmdb_show_id, :integer, null: false
      t.column :name, :string, null: false
      t.column :country_id, :integer
      t.column :language_id, :integer
      t.column :status_id, :integer
      t.column :type_id, :integer
      t.column :rating, :decimal, precision: 4, scale: 2
      t.column :start_date, :date
      t.column :end_date, :date
      t.column :summary, :string
      t.column :runtime, :integer
      t.column :popularity, :integer, default: 0
      t.column :in_production, :boolean, default: false
      t.column :homepage, :string
      t.timestamps
    end
  end

  def down
    drop_table :shows if table_exists? :shows
  end
end
