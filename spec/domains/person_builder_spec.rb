require 'spec_helper'

RSpec.describe PersonBuilder do

  let!(:builder) { PersonBuilder.new }
  let!(:person) { create :person }
  let!(:show) { create :show }
  let!(:person_show) { create :people_show, show: show, person: person }
  let!(:person_response) {JSON.parse(File.read("#{::Rails.root}/spec/fixtures/api_docs/tmdb_responses/person_response.json")).with_indifferent_access}

  describe '#person_from_response' do
    it 'should assign the values from response' do
      result = builder.person_from_response(response: person_response, person: person)
      expect(result.name).to eq 'Bryan Cranston'
      expect(result.tmdb_person_id).to eq 17419
      expect(result.image_url).to eq '/uwGQELv3FGIGm2KU20tOkcKQ54E.jpg'
      expect(result.dob).to eq Date.new(1956, 3, 7)
      expect(result.death_date).to be_nil
      expect(result.place_of_birth).to eq 'San Fernando Valley, California, USA'
      expect(result.biography).to eq person[:biography]
      expect(result.gender).to eq  AppConstants::MALE
      expect(result.popularity).to eq 13
      expect(result.homepage).to eq 'http://www.bryancranston.com/'
    end

    it 'should initialize new record if none passed' do
      result = builder.person_from_response(response: person_response, person: nil)
      expect(result.new_record?).to be true
    end
  end

  describe '#build_person_show' do
    it 'should assign the values from response' do
      result = builder.build_person_show(person: person, show: show, person_show: person_show, credits: {id: 'A'})
      expect(result.person_id).to eq person.id
      expect(result.show_id).to eq show.id
      expect(result.tmdb_credit_id).to eq 'A'
    end

    it 'should return person show with character' do
      expect(person_show.character).to eq 'Char Name'
      result = builder.build_person_show(person: person, show: show, person_show: person_show, credits: {character: 'Test Char 1'})
      expect(result.character).to eq 'Test Char 1'
    end

    it 'should return person_show with job' do
      expect(person_show.job).to eq 'Job Name'
      result = builder.build_person_show(person: person, show: show, person_show: person_show, credits: {job: 'Test Job 1'})
      expect(result.job).to eq 'Test Job 1'
    end

    it 'should initialize new record if none passed' do
      result = builder.build_person_show(person: person, show: show, person_show: nil)
      expect(result.new_record?).to be true
    end
  end

  describe '#assign_credits' do
    it 'should return hash with key id if credit id is creator' do
      response = builder.assign_credits(person_response: {}, credit_id: PeopleShow::CREATOR)
      expect(response.keys).to match_array [:id]
      expect(response.values).to match_array [PeopleShow::CREATOR]
    end

    it 'should return hash with job and character' do
      response = builder.assign_credits(person_response: {job: 'Job Name', character: 'Char name', credit_id: 'A'}.with_indifferent_access, credit_id: 'A')
      expect(response.keys).to match_array [:id, :character, :job]
      expect(response.values).to eq ['A', 'Job Name', 'Char name']
    end
  end

  def setup_response
  end

end
