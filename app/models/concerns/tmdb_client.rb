class TmdbClient

  LIMIT_REACHED = 429
  API_ERRORS = [400, 401, 500, 404]


  def initialize
    @url = ENV_PROPS['tmdb.url']
    @conn = Faraday.new
  end

  def get_tmdb_record(tmdb_show_id: nil, tmdb_person_id: nil)
    if tmdb_show_id
      uri = "/tv/#{tmdb_show_id}"
      append = true
    elsif tmdb_person_id
      uri = "/person/#{tmdb_person_id}"
    end
    url = get_url(uri: uri, append: append)
    _get(url: url)
  end

  private

  attr_accessor :url, :conn

  def _get(url:)
    Rails.logger.info "Get Details for: #{url}"
    response = conn.get url
    validate_response(response: response)
  end

  def get_url(uri:, append: false)
    api_key = '?api_key=' + ENV_PROPS['tmdb.api_key']
    base_url = url + uri + api_key
    base_url = base_url + '&append_to_response=credits,videos,images' if append
    base_url
  end

  def validate_response(response:)
    body = JSON.parse(response.body)
    raise Exceptions::AppExceptions::ApiResponseError.new(message: body['status_message']) if API_ERRORS.include? response.status
    raise Exceptions::AppExceptions::RateLimitError if LIMIT_REACHED == response.status
    body
  end

end