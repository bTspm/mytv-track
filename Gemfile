source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
#Use Rubocop for better coding
gem 'rubocop', '~> 0.39.0'
# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  gem 'better_errors', '~> 2.1', '>= 2.1.1'
  gem 'quiet_assets'
end

group :development, :test do
  gem 'sqlite3'
  gem 'byebug'
  gem 'awesome_print'
end

group :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'rspec', '~> 3.6'
  gem 'factory_girl_rails'
  gem 'simplecov'
  gem 'rspec-rails', '~> 3.5'
  gem 'database_cleaner', '~> 1.5', '>= 1.5.3'
end

group :production do
  gem 'pg'
end

# API's
gem 'faraday', '~> 0.12.1'
gem 'nokogiri', '~> 1.7', '>= 1.7.2'
gem 'webmock', '~> 2.1'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

#UI and Forms
gem 'bootstrap_form'
gem 'bootstrap-sass'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.2'

#Pagination
gem 'kaminari'

gem 'annotate'