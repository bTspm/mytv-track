# == Schema Information
#
# Table name: countries
#
#  id   :integer          not null, primary key
#  name :string           not null
#  iso  :string           not null
#

FactoryGirl.define do
  factory :country do
    name 'United States'
    iso 'USA'
  end
end
