module EagerLoadable
  class EagerLoader
    def initialize(coll, *assocs)
      @collection = coll
      @associations = assocs
    end

    def self.load(collection, *associations)
      preloader = new(collection, *associations)
      preloader.load
      collection
    end

    private

    attr_reader :associations

    def true_collection?
      !@collection.is_a?(ActiveRecord::Base)
    end

    def collection
      true_collection? ? @collection : [@collection].flatten!
    end

    def load
      ActiveRecord::Associations::Preloader.new.preload(collection, associations)
    end

  end

  def eager_load(collection, *associations)
    EagerLoader.load(collection, *associations)
  end
end
