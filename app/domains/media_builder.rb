class MediaBuilder
  def media_from_response(media: nil, show_id:, path:, media_type:)
    media ||= initialize_image
    media.tap {|r|
      r.show_id = show_id
      r.path = path
      r.media_type = media_type
    }
  end

  private

  def initialize_image
    Medium.new
  end
end