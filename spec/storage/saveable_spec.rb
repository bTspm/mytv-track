require 'spec_helper'

describe Storage::Saveable do

  class DummyClass
  end

  let!(:dummy_class) { DummyClass.new.extend(Storage::Saveable) }
  let(:country) { create :country }

  describe '#_save' do
    it 'should create and return the record' do
      country = Country.new({ name: 'Country Name', iso: 'TEST' })
      expect{ dummy_class._save(record: country) }.to change{ Country.count }.by 1
      expect(Country.find_by_iso('TEST').name).to eq 'Country Name'
    end

    it 'should update and return the record' do
      expect(country.name).to eq 'United States'
      country.name = 'Country Name'
      expect{ dummy_class._save(record: country) }.to change{ Country.count }.by 0
      expect(Country.find_by_iso(country.iso).name).to eq 'Country Name'
    end

    it 'should raise an error record invalid if required field is missing' do
      country = Country.new({ name: nil }) #ActiveRecord::RecordInvalid
      expect{ dummy_class._save(record: country) }.to raise_error(Exceptions::AppExceptions::RecordInvalid)
    end

    it 'should raise an error if the not null column is missing' do
      country.id = nil #ActiveRecord::StatementInvalid
      expect{ dummy_class._save(record: country) }.to raise_error(Exceptions::AppExceptions::RecordInvalid)
    end
  end

end