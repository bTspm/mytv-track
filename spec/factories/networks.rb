# == Schema Information
#
# Table name: networks
#
#  id   :integer          not null, primary key
#  name :string           not null
#

FactoryGirl.define do
  factory :network do
    name 'CBS'
  end
end
