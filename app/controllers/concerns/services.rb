module Services
  extend ActiveSupport::Concern
  included do

    def shows_service
      @shows_service ||= Services::ShowsService.new
    end

    def ref_data_service
      @ref_data_service ||= Services::RefDataService.new
    end

    def person_service
      @person_service ||= Services::PersonService.new
    end

  end
end