# == Schema Information
#
# Table name: shows
#
#  id            :integer          not null, primary key
#  tmdb_show_id  :integer          not null
#  name          :string           not null
#  country_id    :integer
#  language_id   :integer
#  status_id     :integer
#  type_id       :integer
#  rating        :decimal(4, 2)
#  start_date    :date
#  end_date      :date
#  summary       :string
#  runtime       :integer
#  popularity    :integer          default("0")
#  in_production :boolean          default("f")
#  homepage      :string
#  created_at    :datetime
#  updated_at    :datetime
#

describe Status do

  subject(:show) { create :show }

  describe '#record validity' do
    it 'should not be valid - presence' do
      record = build(:show, name: nil, tmdb_show_id: nil)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ["Name can't be blank", "Tmdb show can't be blank"]
    end

    it 'should not be valid - uniqueness' do
      create :show, name: 'Test', tmdb_show_id: 999
      record = build(:show, name: 'Test', tmdb_show_id: 999)
      expect(record.valid?).to be false
      expect(record.errors.full_messages).to match_array ['Name has already been taken', 'Tmdb show has already been taken']
    end

    it 'should be valid' do
      expect(build(:show, name: 'Test - 1')).to be_valid
    end
  end

end
