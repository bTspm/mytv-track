# == Schema Information
#
# Table name: people_shows
#
#  id             :integer          not null, primary key
#  person_id      :integer          not null
#  show_id        :integer          not null
#  tmdb_credit_id :string           not null
#  character      :string
#  job            :string
#

class PeopleShow < ActiveRecord::Base

  belongs_to :person
  belongs_to :show

  CREATOR = 'Creator'

  validates :person_id, :show_id, presence: true
  validates :tmdb_credit_id, presence: true, uniqueness: { scope: [:person_id, :show_id] }
  validate :job_or_character

  def job_or_character
    if character.nil? && job.nil? && tmdb_credit_id != CREATOR
      errors.add(:base, 'Need at-least job or character')
    end
  end

end
