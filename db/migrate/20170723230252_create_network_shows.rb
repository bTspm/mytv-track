class CreateNetworkShows < ActiveRecord::Migration
  def up
    create_table :networks_shows do |t|
      t.integer :network_id
      t.integer :show_id
    end
  end

  def down
    drop_table :networks_shows if table_exists? :networks_shows
  end

end
