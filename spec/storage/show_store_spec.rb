require 'spec_helper'
require 'show_store'

RSpec.describe Storage::ShowStore do

  let!(:store) {Storage::ShowStore.new}
  let!(:show) {create :show, tmdb_show_id: 1,
                      genres: [(create :genre)],
                      networks: [(create :network)],
                      production_companies: [(create :production_company)]}
  let!(:poster) {create :medium, show: show, path: 'POSTER', media_type: AppConstants::POSTER}
  let!(:trailer) {create :medium, show: show, path: 'TRAILER', media_type: AppConstants::TRAILER}
  let!(:backdrop) {create :medium, show: show, path: 'BACKDROP', media_type: AppConstants::BACKDROP}

  describe '#get_show' do
    it 'should return show on availability' do
      response = store.get_show(tmdb_show_id: 1)
      expect(response.tmdb_show_id).to eq show.tmdb_show_id
      expect(response.name).to eq show.name

      # Passing last + 1 id would not be available.
      expect(store.get_show(tmdb_show_id: Show.last.tmdb_show_id + 1)).to be_nil
    end
  end

  describe '#save_record' do
    it 'should create and return show' do
      attrs = show.attributes.merge({id: nil, tmdb_show_id: 999, name: 'TTTT'})
      show = Show.new(attrs)
      response = store.save_record(record: show)
      expect(response.tmdb_show_id).to eq 999
      expect(response.name).to eq 'TTTT'
    end
    it 'should update and return show' do
      expect(show.name).to eq 'Test Show'
      expect(show.tmdb_show_id).to eq 1
      show.name = 'TTT1'
      response = store.save_record(record: show)
      expect(response.tmdb_show_id).to eq 1
      expect(response.name).to eq 'TTT1'
    end
  end

  describe '#get_shows' do
    context 'sort by' do
      it 'should return shows sorted by name' do
        show1 = create :show, tmdb_show_id: 12, name: 'AAA'
        result = store.get_shows
        expect(result.size).to eq 2

        #Checking Order
        expect(result.first.id).to eq show1.id
        expect(result.last.id).to eq show.id
      end
      it 'should return shows sorted by rating' do
        show1 = create :show, tmdb_show_id: 12, name: 'AAA', rating: 10
        result = store.get_shows(options: {sort_by: AppConstants::TOP_RATED})
        expect(result.size).to eq 2

        #Checking Order
        expect(result.first.id).to eq show1.id
        expect(result.last.id).to eq show.id
      end
      it 'should return shows sorted by start date' do
        show1 = create :show, tmdb_show_id: 12, name: 'AAA', start_date: Date.new(9999, 01, 01)
        result = store.get_shows(options: {sort_by: AppConstants::LATEST_SHOWS})
        expect(result.size).to eq 2

        #Checking Order
        expect(result.first.id).to eq show1.id
        expect(result.last.id).to eq show.id
      end
      it 'should return shows sorted by most popular' do
        show1 = create :show, tmdb_show_id: 12, name: 'AAA', popularity: 100
        result = store.get_shows(options: {sort_by: AppConstants::MOST_POPULAR})
        expect(result.size).to eq 2

        #Checking Order
        expect(result.first.id).to eq show1.id
        expect(result.last.id).to eq show.id
      end
    end
    context 'filter options' do
      it 'should return shows filtered by params' do
        expect(store.get_shows(filter_params: {country_id: show.country_id}).first.id).to eq show.id
        expect(store.get_shows(filter_params: {type_id: show.type_id}).first.id).to eq show.id
        expect(store.get_shows(filter_params: {language_id: show.language_id}).first.id).to eq show.id
        expect(store.get_shows(filter_params: {status_id: show.status_id}).first.id).to eq show.id
        expect(store.get_shows(filter_params: {network_id: show.networks.first.id}).first.id).to eq show.id
        expect(store.get_shows(filter_params: {genre_id: show.genres.first.id}).first.id).to eq show.id
        expect(store.get_shows(filter_params: {production_company_id: show.production_companies.first.id}).first.id).to eq show.id
      end
      it 'should return show with search query' do
        expect(store.get_shows(options: {search_query: show.name}).first.id).to eq show.id
      end
    end
    context 'limit / pagination' do
      it 'should return shows when limit is passed' do
        show1 = create :show, tmdb_show_id: 12, name: 'AAA'
        result = store.get_shows(options: {limit: 1})
        expect(result.size).to eq 1
        expect(result.first.id).to eq show1.id #Ordered by name.
      end
      it 'should return shows with pagination limit' do
        30.times do |index|
          create :show, name: index + 5, tmdb_show_id: index + 5
        end
        result = store.get_shows
        expect(result.size).to eq AppConstants::PAGINATION_COUNT
        expect(result.total_count).to eq 31
      end
    end
  end

  describe '#get_show_media' do
    it 'should return shows media based on mediatype' do
      expect(store.get_show_media(show: show, media_type: AppConstants::POSTER).path).to eq 'POSTER'
      expect(store.get_show_media(show: show, media_type: AppConstants::TRAILER).path).to eq 'TRAILER'
      expect(store.get_show_media(show: show, media_type: AppConstants::BACKDROP).path).to eq 'BACKDROP'
    end
  end

end