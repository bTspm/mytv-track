class ShowsController < ApplicationController

  def index
    assign_selection_lists
    @shows = shows_service.get_shows(filter_params: filter_params, options: options)
  end

  private

  def filter_params
    params.permit(:country_id, :network_id, :genre_id, :type_id, :language_id, :status_id)
  end

  def options
    params.permit(:page, :sort_by, :search_query)
  end

  def assign_selection_lists
    @filter_lists = ref_data_service.get_ref_data_lists
  end

end