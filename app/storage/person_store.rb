module Storage
  class PersonStore
    include Saveable

    def get_person(tmdb_person_id:)
      Person.where(tmdb_person_id: tmdb_person_id).first
    end

    def get_person_show(tmdb_credit_id:, show_id:, person_id:)
      PeopleShow.where(tmdb_credit_id: tmdb_credit_id, show_id: show_id, person_id: person_id).first
    end

    def save_record(record:)
      _save(record: record)
    end
  end
end